#ifndef FX_H
#define FX_H

#include <string>

#include "D3DUtil.h"
#include "d3d.h"
#include "ShaderTypes.h"
#include "SimpleMath.h"


class MyFX;
class MyD3D;

//when you use resources you get a handle to them
//when you've finished you have to release them or leak memory
class FXData
{
public:
	ID3D11InputLayout* mpInputLayout = nullptr;
	ID3D11Buffer* mpVB = nullptr;
	ID3D11Buffer* mpIB = nullptr;
	ID3D11VertexShader* mpVertexShader = nullptr;
	ID3D11PixelShader* mpPixelShader = nullptr;
	unsigned int mVertSize = 0;

	void Release();
	bool Init(MyD3D& d3d, const std::string& vsPath, const std::string& psPath, unsigned int vertSize, const D3D11_INPUT_ELEMENT_DESC *vertDesc, const int numElements);
	void Draw(MyD3D& d3d, int numV, const DirectX::SimpleMath::Vector4& colour = DirectX::SimpleMath::Vector4(1,1,1,1));
	void BuildGeometryBuffers(MyD3D& d3d);
};

class Camera
{
public:
	DirectX::SimpleMath::Matrix mWorld, mView, mProj;
	DirectX::SimpleMath::Vector3 mCamPos, mCamTgt, mCamUp;
	float fieldOfView, nearZ, farZ;

	Camera()
		: mCamPos(0, 0, -10), mCamTgt(0, 0, 0), mCamUp(0, 1, 0), fieldOfView(0.25f*PI), nearZ(1), farZ(1000.f)
	{}
	void Update(MyD3D& d3d);
};


//everything FX related (shaders and things) - this is likely to grow
class MyFX
{
public:
	ID3D11Buffer* gpGfxConstsPerObjBuffer;
	GfxParamsPerObj gGfxData;
	Camera mCamera;
	MyD3D& mD3D;

	MyFX(MyD3D& d3d)
		:mD3D(d3d) 
	{}
	void UpdateConstsPerObj(const DirectX::SimpleMath::Vector4& colour);
	void SetConstantBufferForShader();
	void Release() {
		ReleaseCOM(gpGfxConstsPerObjBuffer);
		gpGfxConstsPerObjBuffer = nullptr;
	}

	//we've loaded in a "blob" of compiled shader code, it needs to be set up on the gpu as a pixel shader
	static void CreatePixelShader(char* pBuff, unsigned int buffSz, ID3D11PixelShader* &pPS, ID3D11Device& device);
	//we've loaded in a "blob" of compiled shader code, it needs to be set up on the gpu as a vertex shader
	static void CreateVertexShader(char* pBuff, unsigned int buffSz, ID3D11VertexShader* &pVS, ID3D11Device& device);
	//the input assembler needs to know what is in the vertex buffer, how to get the data out and which vertex shader to give it to
	static void CreateInputLayout(const D3D11_INPUT_ELEMENT_DESC vdesc[], int numElements, char* pBuff, unsigned int buffSz, ID3D11InputLayout** pLayout, ID3D11Device& device);
	//if we want to give extra information to the shaders then it has to go in a constant buffer
	//once a shader runs, to render a leg or something, the data can't change until it's finished
	//so it's always a constant
	static void CreateConstantBuffer(UINT sizeOfBuffer, ID3D11Buffer **pBuffer, ID3D11Device& device);
	//different hardware supports different instructions, lines of code, number of constants, etc
	//we cheat a bit and just go for shader model 5 which is really common and powerful
	static void CheckShaderModel5Supported(ID3D11Device& device);
	//used to load a file as a chunk of binary, the buffer returned needs to be deleted
	static char* ReadAndAllocate(const std::string& fileName, unsigned int& bytesRead);

};



#endif
