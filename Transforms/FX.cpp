#include <fstream>

#include "D3D.h"
#include "D3DUtil.h"
#include "FX.h"

using namespace std;
using namespace DirectX;
using namespace DirectX::SimpleMath;

bool FXData::Init(MyD3D& d3d, const std::string& vsPath, const string& psPath, unsigned int vertSize, const D3D11_INPUT_ELEMENT_DESC *vertDesc, const int numElements)
{
	//check the gpu is good enough
	MyFX::CheckShaderModel5Supported(d3d.GetDevice());

	MyFX::CreateConstantBuffer(sizeof(GfxParamsPerObj), &d3d.GetFX().gpGfxConstsPerObjBuffer, d3d.GetDevice());

	//load in a pre-compiled vertex shader
	char* pBuff = nullptr;
	unsigned int bytes = 0;
	pBuff = MyFX::ReadAndAllocate(vsPath, bytes);
	MyFX::CreateVertexShader(pBuff, bytes, mpVertexShader, d3d.GetDevice());
	//create a link between our data and the vertex shader
	mVertSize = vertSize;
	MyFX::CreateInputLayout(vertDesc, numElements, pBuff, bytes, &mpInputLayout, d3d.GetDevice());
	delete[] pBuff;

	//load in a pre-compiled pixel shader	
	pBuff = MyFX::ReadAndAllocate(psPath, bytes);
	MyFX::CreatePixelShader(pBuff, bytes, mpPixelShader, d3d.GetDevice());
	delete[] pBuff;

	return true;

}


void FXData::Release()
{
	ReleaseCOM(mpVertexShader);
	ReleaseCOM(mpPixelShader);
	ReleaseCOM(mpVB);
	ReleaseCOM(mpIB);
	ReleaseCOM(mpInputLayout);
}
void FXData::Draw(MyD3D& d3d, int numV, const Vector4& colour)
{
	d3d.InitInputAssembler(mpInputLayout, mpVB, mVertSize, mpIB);
	d3d.GetDeviceCtx().VSSetShader(mpVertexShader, nullptr, 0);
	d3d.GetDeviceCtx().PSSetShader(mpPixelShader, nullptr, 0);
	
	d3d.GetFX().UpdateConstsPerObj(colour);
	d3d.GetFX().SetConstantBufferForShader();
	d3d.GetDeviceCtx().DrawIndexed(numV, 0, 0);
}

//setup our quad
void FXData::BuildGeometryBuffers(MyD3D& d3d)
{
	// Create vertex buffer for a quad (two triangle square)
	VertexPosColour vertices[] =
	{
		{ Vector3(-0.5f, -0.5f, 0.f), Colors::White   },
		{ Vector3(-0.5f, +0.5f, 0.f), Colors::Black },
		{ Vector3(+0.5f, +0.5f, 0.f), Colors::Red },
		{ Vector3(+0.5f, -0.5f, 0.f), Colors::Green }
	};

	MyD3D::CreateVertexBuffer(sizeof(VertexPosColour) * 4, vertices, mpVB, d3d.GetDevice());


	// Create the index buffer

	UINT indices[] = {
		// front face
		0, 1, 2,
		0, 2, 3
	};

	MyD3D::CreateIndexBuffer(sizeof(UINT) * 6, indices, mpIB, d3d.GetDevice());
}




void MyFX::CheckShaderModel5Supported(ID3D11Device& device)
{
	D3D_FEATURE_LEVEL featureLevel = device.GetFeatureLevel();
	bool featureLevelOK = true;
	switch (featureLevel)
	{
	case D3D_FEATURE_LEVEL_11_1:
	case D3D_FEATURE_LEVEL_11_0:
		break;
	default:
		featureLevelOK = false;
	}

	if (!featureLevelOK)
	{
		DBOUT("feature level too low for shader model 5");
		assert(false);
	}
}

void MyFX::CreateConstantBuffer(UINT sizeOfBuffer, ID3D11Buffer **pBuffer, ID3D11Device& device)
{
	// Create the constant buffers for the variables defined in the vertex shader.
	D3D11_BUFFER_DESC constantBufferDesc;
	ZeroMemory(&constantBufferDesc, sizeof(D3D11_BUFFER_DESC));

	constantBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	constantBufferDesc.ByteWidth = sizeOfBuffer;
	constantBufferDesc.CPUAccessFlags = 0;
	constantBufferDesc.Usage = D3D11_USAGE_DEFAULT;

	HR(device.CreateBuffer(&constantBufferDesc, nullptr, pBuffer));

}

char* MyFX::ReadAndAllocate(const string& fileName, unsigned int& bytesRead)
{
	//open the file for reading
	ifstream infile;
	infile.open(fileName, ios::binary | ios::in);
	if (!infile.is_open() || infile.fail())
	{
		DBOUT("failed to open file: " << fileName);
		assert(false);
		return nullptr;
	}

	//read the whole contents
	infile.seekg(0, ios::end);
	ifstream::streampos size = infile.tellg();
	if (size > INT_MAX || size <= 0)
	{
		DBOUT("failed to get size of file: " << fileName);
		assert(false);
	}
	char* pBuff = new char[(int)size];
	infile.seekg(0, ios::beg);
	infile.read(pBuff, size);
	if (infile.fail())
	{
		DBOUT("failed to read file: " << fileName);
		assert(false);
	}
	infile.close();
	bytesRead = (int)size;
	return pBuff;
}


void MyFX::CreateInputLayout(const D3D11_INPUT_ELEMENT_DESC vdesc[], int numElements, char* pBuff, unsigned int buffSz, ID3D11InputLayout** pLayout, ID3D11Device& device)
{
	assert(pBuff);
	HR(device.CreateInputLayout(vdesc, numElements, pBuff, buffSz, pLayout));

}

void MyFX::CreateVertexShader(char* pBuff, unsigned int buffSz, ID3D11VertexShader* &pVS, ID3D11Device& device)
{
	assert(pBuff);
	HR(device.CreateVertexShader(pBuff,
		buffSz,
		nullptr,
		&pVS));
	assert(pVS);
}

void MyFX::UpdateConstsPerObj(const Vector4& colour)
{
	gGfxData.colour = colour;
	gGfxData.wvp = mCamera.mWorld * mCamera.mView * mCamera.mProj;
	mD3D.GetDeviceCtx().UpdateSubresource(gpGfxConstsPerObjBuffer, 0, nullptr, &gGfxData, 0, 0);
}

void MyFX::SetConstantBufferForShader()
{
	mD3D.GetDeviceCtx().VSSetConstantBuffers(0, 1, &gpGfxConstsPerObjBuffer);
}

void MyFX::CreatePixelShader(char* pBuff, unsigned int buffSz, ID3D11PixelShader* &pPS, ID3D11Device& device)
{
	assert(pBuff);
	HR(device.CreatePixelShader(pBuff,
		buffSz,
		nullptr,
		&pPS));
	assert(pPS);
}

void Camera::Update(MyD3D & d3d)
{
	MyD3D::CreateProjectionMatrix(mProj, fieldOfView, d3d.GetAspectRatio(), nearZ, farZ);
	MyD3D::CreateViewMatrix(mView, mCamPos, mCamTgt, mCamUp);
}
