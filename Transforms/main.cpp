#include <windows.h>
#include <string>
#include <cassert>
#include <d3d11.h>
#include <D3D11SDKLayers.h>

#include "WindowUtils.h"
#include "D3DUtil.h"
#include "D3D.h"
#include "SimpleMath.h"
#include "ShaderTypes.h"
#include "FX.h"

using namespace std;
using namespace DirectX;
using namespace DirectX::SimpleMath;

void Translate(VertexPosColour verts[], int numV, const Vector3& offset)
{
	for (int i = 0; i < numV; ++i)
		verts[i].Pos += offset;
}

void Scale(VertexPosColour verts[], int numV, const Vector3& scale)
{
	for (int i = 0; i < numV; ++i)
		verts[i].Pos *= scale;
}


class Cross : public FXData
{
public:
	void BuildGeometryBuffers(MyD3D& d3d)
	{
		//cross
		VertexPosColour vertices3[] =
		{
			{ Vector3(.2f, 0.f, 0.f), Colors::White },
			{ Vector3(0.f, 0.2f, 0.f), Colors::White },
			{ Vector3(0.4f,.5f, 0.f), Colors::White },
			{ Vector3(0.f, .8f, 0.f), Colors::White },
			{ Vector3(.2f, 1.f, 0.f), Colors::White },
			{ Vector3(.5f, .6f, 0.f), Colors::White },
			{ Vector3(.8f, 1.f, 0.f), Colors::White },
			{ Vector3(1.f, .8f, 0.f), Colors::White },
			{ Vector3(.6f, .5f, 0.f), Colors::White },
			{ Vector3(1.f, .2f, 0.f), Colors::White },
			{ Vector3(.8f, 0.f, 0.f), Colors::White },
			{ Vector3(.5f, .4f, 0.f), Colors::White }
		};
		Scale(vertices3, 12, Vector3(0.75f, 0.75f, 0.75f));
		Translate(vertices3, 12, Vector3(0, -0.75, 0));
		MyD3D::CreateVertexBuffer(sizeof(VertexPosColour) * 12, vertices3, mpVB,d3d.GetDevice());
		UINT indices3[] = {
			0,1,2,
			0,2,11,
			2,3,4,
			2,4,5,
			11,2,5,
			11,5,8,
			8,5,6,
			8,6,7,
			10,11,8,
			10,8,9
		};
		MyD3D::CreateIndexBuffer(sizeof(UINT) * 30, indices3, mpIB,d3d.GetDevice());
	}
	void Draw(MyD3D& d3d, const Vector4& colour)
	{
		FXData::Draw(d3d, 30, colour);
	}
};


const int MAX_ROTS = 80;
float gAngles[MAX_ROTS];
float gScale = 1, gScaleInc = 1;

//things we want to do once at the start
//note - each geometry instances is loading its own vertex/pixel shader, input layout - WASTEFULL!!
void InitGame(MyD3D& d3d, Cross& cross)
{
	if (!cross.Init(d3d, "../bin/data/SimpleVS.cso", "../bin/data/SimplePS.cso", sizeof(VertexPosColour), VertexPosColour::sVertexDesc, VertexPosColour::NUM_ELEMENTS))
		assert(false);
	cross.BuildGeometryBuffers(d3d);
	float inc = (2 * PI) / MAX_ROTS;
	for (int i = 0; i < MAX_ROTS; ++i)
		gAngles[i] = inc * i;

}

//tidying up at the end
void ReleaseGame(Cross& cross)
{
	cross.Release();
}


//logic - runs all the time
void Update(float dTime, MyD3D& d3d, Cross& cross)
{
	d3d.GetFX().mCamera.Update(d3d);
	for (int i = 0; i < MAX_ROTS; ++i)
		gAngles[i] += dTime;
	
	gScale += gScaleInc * dTime;
	if (gScale > 2 || gScale < 0.25f)
		gScaleInc *= -1;
}



//paint the screen - runs all the time
void Render(float dTime, MyD3D& d3d, Cross& cross)
{
	d3d.BeginRender(Colors::Blue);

	Matrix trans;
	for (int i = 0; i < MAX_ROTS; ++i)
	{
		trans = Matrix::CreateTranslation(sinf(gAngles[i]) * 3, cosf(gAngles[i]) * 3, 0);
		d3d.GetFX().mCamera.mWorld = trans;
		Vector4 colour = Colors::White + (Colors::Black-Colors::White) * ((float)i/MAX_ROTS);
		cross.Draw(d3d, colour);
	}
	
	Matrix scale = Matrix::CreateScale(gScale, gScale, 1);
	d3d.GetFX().mCamera.mWorld = scale;
	cross.Draw(d3d, Colors::Magenta);

	d3d.EndRender();
}

//if ALT+ENTER or resize or drag window we might want do
//something like pause the game perhaps, but we definitely
//need to let D3D know what's happened (OnResize_Default).
void OnResize(int screenWidth, int screenHeight, MyD3D& d3d)
{

	d3d.OnResize_Default(screenWidth, screenHeight);
}

//messages come from windows all the time, should we respond to any specific ones?
LRESULT CALLBACK MainWndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	//do something game specific here
	switch (msg)
	{
		// Respond to a keyboard event.
	case WM_CHAR:
		switch (wParam)
		{
		case 27:
		case 'q':
		case 'Q':
			PostQuitMessage(0);
			return 0;
		}
	}
	//default message handling (resize window, full screen, etc)
	return WinUtil::DefaultMssgHandler(hwnd, msg, wParam, lParam);
}

//main entry point for the game
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE prevInstance,
				   PSTR cmdLine, int showCmd){
	int w(1024), h(768);
	//int defaults[] = { 640,480, 800,600, 1024,768, 1280,1024 };
		//WinUtil::ChooseRes(w, h, defaults, 4);

	if(!WinUtil::Get().InitMainWindow(800,600, hInstance, "MyApp", MainWndProc))
		assert(false);

	MyD3D d3d;
	if (!d3d.InitDirect3D(OnResize))
		assert(false);
	WinUtil::Get().SetD3D(d3d);

	Cross cross;

	InitGame(d3d, cross);
	bool canUpdateRender;
	float dTime = 0;
	while (WinUtil::Get().BeginLoop(canUpdateRender))
	{
		if (canUpdateRender)
		{
			Update(dTime, d3d, cross);
			Render(dTime, d3d, cross);
		}
		dTime = WinUtil::Get().EndLoop(canUpdateRender);
	}

	ReleaseGame(cross);
	d3d.ReleaseD3D(true);
	return 0;
}